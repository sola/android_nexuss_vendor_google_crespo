ifeq ($(TARGET_PRODUCT),full_crespo)
ifneq ($(OTA_NEXUSS),true)

LOCAL_PATH := vendor/google/crespo

PRODUCT_PACKAGES += \
	BooksTablet \
	CalendarGoogle \
	CameraGoogle \
	CarHomeGoogle \
	ChromeBookmarksSyncAdapter \
	DeskClockGoogle \
	EmailGoogle \
	ExchangeGoogle \
	GalleryGoogle \
	GenieWidget \
	Gmail \
	GoogleBackupTransport \
	GoogleContactsSyncAdapter \
	GoogleEarth \
	GoogleFeedback \
	GoogleLoginService \
	GooglePartnerSetup \
	GoogleQuickSearchBox \
	GoogleServicesFramework \
	GoogleTTS \
	Maps \
	MediaUploader \
	Microbes \
	Music2 \
	NetworkLocation \
	OneTimeInitializer \
	Phonesky \
	PlusOne \
	SetupWizard \
	Street \
	TagGoogle \
	Talk \
	Thinkfree \
	VideoEditorGoogle \
	Videos \
	VoiceSearch \
	YouTube \
	googlevoice \
	talkback 


### Add
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/features.xml:system/etc/permissions/features.xml \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
	$(LOCAL_PATH)/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml \
	$(LOCAL_PATH)/proprietary/system/etc/updatecmds/google_generic_update.txt:system/etc/updatecmds/google_generic_update.txt \
	$(LOCAL_PATH)/proprietary/system/media/bootanimation.zip:system/media/bootanimation.zip \
	$(LOCAL_PATH)/proprietary/system/media/PFFprec_600.emd:system/media/PFFprec_600.emd \
	$(LOCAL_PATH)/proprietary/system/media/LMprec_508.emd:system/media/LMprec_508.emd \
	$(LOCAL_PATH)/proprietary/system/media/video/Sunset.240p.mp4:system/media/video/Sunset.240p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/AndroidInSpace.240p.mp4:system/media/video/AndroidInSpace.240p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/AndroidInSpace.480p.mp4:system/media/video/AndroidInSpace.480p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/Sunset.480p.mp4:system/media/video/Sunset.480p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/Disco.240p.mp4:system/media/video/Disco.240p.mp4 \
	$(LOCAL_PATH)/proprietary/system/media/video/Disco.480p.mp4:system/media/video/Disco.480p.mp4 \
	$(LOCAL_PATH)/proprietary/system/framework/com.google.android.media.effects.jar:system/framework/com.google.android.media.effects.jar \
	$(LOCAL_PATH)/proprietary/system/framework/com.google.android.maps.jar:system/framework/com.google.android.maps.jar \
	$(LOCAL_PATH)/proprietary/system/framework/com.google.widevine.software.drm.jar:system/framework/com.google.widevine.software.drm.jar \
	$(LOCAL_PATH)/proprietary/system/lib/libvoicesearch.so:system/lib/libvoicesearch.so \
	$(LOCAL_PATH)/proprietary/system/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so \
	$(LOCAL_PATH)/proprietary/system/lib/libfrsdk.so:system/lib/libfrsdk.so \
	$(LOCAL_PATH)/proprietary/system/vendor/firmware/cypress-touchkey.bin:system/vendor/firmware/cypress-touchkey.bin \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/drm/libdrmwvmplugin.so:system/vendor/lib/drm/libdrmwvmplugin.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libWVStreamControlAPI_L3.so:system/vendor/lib/libWVStreamControlAPI_L3.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libwvdrm_L3.so:system/vendor/lib/libwvdrm_L3.so \
	$(LOCAL_PATH)/proprietary/system/vendor/lib/libwvm.so:system/vendor/lib/libwvm.so \
	$(LOCAL_PATH)/proprietary/system/lib/libmicrobes_jni.so:system/lib/libmicrobes_jni.so \
	$(LOCAL_PATH)/proprietary/system/lib/libearthmobile.so:system/lib/libearthmobile.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_blkid.so:system/lib/libext2_blkid.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_com_err.so:system/lib/libext2_com_err.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_e2p.so:system/lib/libext2_e2p.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_profile.so:system/lib/libext2_profile.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2_uuid.so:system/lib/libext2_uuid.so \
	$(LOCAL_PATH)/proprietary/system/lib/libext2fs.so:system/lib/libext2fs.so \
	$(LOCAL_PATH)/proprietary/system/lib/libflint_engine_jni_api.so:system/lib/libflint_engine_jni_api.so \
	$(LOCAL_PATH)/proprietary/system/lib/libpicowrapper.so:system/lib/libpicowrapper.so \
	$(LOCAL_PATH)/proprietary/system/lib/libspeexwrapper.so:system/lib/libspeexwrapper.so \
	$(LOCAL_PATH)/proprietary/system/lib/libvideochat_jni.so:system/lib/libvideochat_jni.so \
	$(LOCAL_PATH)/proprietary/system/lib/libvideochat_stabilize.so:system/lib/libvideochat_stabilize.so \
	$(LOCAL_PATH)/proprietary/system/lib/libWVphoneAPI.so:system/lib/libWVphoneAPI.so \
	$(LOCAL_PATH)/proprietary/system/lib/libgcomm_jni.so:system/lib/libgcomm_jni.so

endif
endif

