#!/bin/sh

mkdir framework
adb pull /system/framework ./framework/
./apktool if ./framework/framework-res.apk

#####
java -jar baksmali-1.3.2.jar -d ./framework -x ./framework/com.google.widevine.software.drm.odex
java -jar apktool.jar d ./framework/com.google.widevine.software.drm.jar "./apk.out"
mkdir ./apk.out/smali
cp -a ./out/* ./apk.out/smali/
java -jar apktool.jar b "./apk.out" "./framework/com.google.widevine.software.drm-unsigned.jar"
java -jar signapk.jar -w testkey.x509.pem testkey.pk8 ./framework/com.google.widevine.software.drm-unsigned.jar ./framework/com.google.widevine.software.drm-signed.jar
rm -rf ./apk.out
rm -rf ./out

cp -a ./framework/com.google.widevine.software.drm-signed.jar ./com.google.widevine.software.drm.jar
####
java -jar baksmali-1.3.2.jar -d ./framework -x ./framework/com.google.android.maps.odex
java -jar apktool.jar d ./framework/com.google.android.maps.jar "./apk.out"
mkdir ./apk.out/smali
cp -a ./out/* ./apk.out/smali/
java -jar apktool.jar b "./apk.out" "./framework/com.google.android.maps-unsigned.jar"
java -jar signapk.jar -w testkey.x509.pem testkey.pk8 ./framework/com.google.android.maps-unsigned.jar ./framework/com.google.android.maps-signed.jar
rm -rf ./apk.out
rm -rf ./out

cp -a ./framework/com.google.android.maps-signed.jar ./com.google.android.maps.jar
#####
java -jar baksmali-1.3.2.jar -d ./framework -x ./framework/com.google.android.media.effects.odex
java -jar apktool.jar d ./framework/com.google.android.media.effects.jar "./apk.out"
mkdir ./apk.out/smali
cp -a ./out/* ./apk.out/smali/
java -jar apktool.jar b "./apk.out" "./framework/com.google.android.media.effects-unsigned.jar"
java -jar signapk.jar -w testkey.x509.pem testkey.pk8 ./framework/com.google.android.media.effects-unsigned.jar ./framework/com.google.android.media.effects-signed.jar
rm -rf ./apk.out
rm -rf ./out

cp -a ./framework/com.google.android.media.effects-signed.jar ./com.google.android.media.effects.jar
#####


mkdir tmp
mkdir tmp/app

mkdir app

#######

APPS="CalendarGoogle CarHomeGoogle ChromeBookmarksSyncAdapter DeskClockGoogle EmailGoogle ExchangeGoogle GalleryGoogle GenieWidget Gmail GoogleBackupTransport GoogleContactsSyncAdapter GoogleFeedback GoogleLoginService GooglePartnerSetup GoogleQuickSearchBox GoogleServicesFramework GoogleTTS Microbes NetworkLocation OneTimeInitializer SetupWizard TagGoogle Talk"

for APP in ${APPS}; do

	adb pull /system/app/${APP}.apk ./app/
	adb pull /system/app/${APP}.odex ./app/

	java -Xmx1024m -jar baksmali-1.3.2.jar -d ./framework -c core.jar:core-junit.jar:bouncycastle.jar:ext.jar:framework.jar:android.policy.jar:services.jar:apache-xml.jar:filterfw.jar:com.android.location.provider.jar -x ./app/${APP}.odex
	java -jar apktool.jar d ./app/${APP}.apk "./apk.out"
	mkdir ./apk.out/smali
	cp -a ./out/* ./apk.out/smali/

	if [ ${APP} = "EmailGoogle" ];
	then
		./fixEmailGoogle.sh
	fi

	java -jar apktool.jar b "./apk.out" "./tmp/app/${APP}-unsigned.apk"
	java -jar signapk.jar -w testkey.x509.pem testkey.pk8 ./tmp/app/${APP}-unsigned.apk ./tmp/app/${APP}.apk
	rm ./tmp/app/${APP}-unsigned.apk
	cp -a ./tmp/app/${APP}.apk ./${APP}.apk
	rm -rf ./apk.out
	rm -rf ./out

done

#######

rm -rf ~/apktool
rm -rf tmp
rm -rf framework
rm -rf app

