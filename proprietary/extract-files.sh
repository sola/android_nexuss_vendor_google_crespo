#!/bin/bash
adb pull /system/etc/permissions/com.google.android.media.effects.xml system/etc/permissions/com.google.android.media.effects.xml
adb pull /system/etc/permissions/features.xml system/etc/permissions/features.xml
adb pull /system/etc/permissions/com.google.android.maps.xml system/etc/permissions/com.google.android.maps.xml
adb pull /system/etc/permissions/com.google.widevine.software.drm.xml system/etc/permissions/com.google.widevine.software.drm.xml
adb pull /system/etc/updatecmds/google_generic_update.txt system/etc/updatecmds/google_generic_update.txt
adb pull /system/media/bootanimation.zip system/media/bootanimation.zip
adb pull /system/media/PFFprec_600.emd system/media/PFFprec_600.emd
adb pull /system/media/LMprec_508.emd system/media/LMprec_508.emd
adb pull /system/media/video/Sunset.240p.mp4 system/media/video/Sunset.240p.mp4
adb pull /system/media/video/AndroidInSpace.240p.mp4 system/media/video/AndroidInSpace.240p.mp4
adb pull /system/media/video/AndroidInSpace.480p.mp4 system/media/video/AndroidInSpace.480p.mp4
adb pull /system/media/video/Sunset.480p.mp4 system/media/video/Sunset.480p.mp4
adb pull /system/media/video/Disco.240p.mp4 system/media/video/Disco.240p.mp4
adb pull /system/media/video/Disco.480p.mp4 system/media/video/Disco.480p.mp4
adb pull /system/app/VideoEditorGoogle.apk system/app/VideoEditorGoogle/VideoEditorGoogle.apk
adb pull /system/app/talkback.apk system/app/talkback/talkback.apk
adb pull /system/app/GoogleEarth.apk system/app/GoogleEarth/GoogleEarth.apk
adb pull /system/app/Music2.apk system/app/Music2/Music2.apk
adb pull /system/app/MediaUploader.apk system/app/MediaUploader/MediaUploader.apk
adb pull /system/app/VoiceSearch.apk system/app/VoiceSearch/VoiceSearch.apk
adb pull /system/app/Maps.apk system/app/Maps/Maps.apk
adb pull /system/app/Thinkfree.apk system/app/Thinkfree/Thinkfree.apk
adb pull /system/app/Phonesky.apk system/app/Phonesky/Phonesky.apk
adb pull /system/app/PlusOne.apk system/app/PlusOne/PlusOne.apk
adb pull /system/app/Videos.apk system/app/Videos/Videos.apk
adb pull /system/app/Street.apk system/app/Street/Street.apk
adb pull /system/app/YouTube.apk system/app/YouTube/YouTube.apk
adb pull /system/app/BooksTablet.apk system/app/BooksTablet/BooksTablet.apk
adb pull /system/app/googlevoice.apk system/app/googlevoice/googlevoice.apk
adb pull /system/lib/libvoicesearch.so system/lib/libvoicesearch.so
adb pull /system/lib/libfilterpack_facedetect.so system/lib/libfilterpack_facedetect.so
adb pull /system/lib/libfrsdk.so system/lib/libfrsdk.so
adb pull /system/vendor/firmware/cypress-touchkey.bin system/vendor/firmware/cypress-touchkey.bin
adb pull /system/vendor/lib/drm/libdrmwvmplugin.so system/vendor/lib/drm/libdrmwvmplugin.so
adb pull /system/vendor/lib/libWVStreamControlAPI_L3.so system/vendor/lib/libWVStreamControlAPI_L3.so
adb pull /system/vendor/lib/libwvdrm_L3.so system/vendor/lib/libwvdrm_L3.so
adb pull /system/vendor/lib/libwvm.so system/vendor/lib/libwvm.so
adb pull /system/lib/libmicrobes_jni.so system/lib/libmicrobes_jni.so
adb pull /system/lib/libearthmobile.so system/lib/libearthmobile.so
adb pull /system/lib/libext2_blkid.so system/lib/libext2_blkid.so
adb pull /system/lib/libext2_com_err.so system/lib/libext2_com_err.so
adb pull /system/lib/libext2_e2p.so system/lib/libext2_e2p.so
adb pull /system/lib/libext2_profile.so system/lib/libext2_profile.so
adb pull /system/lib/libext2_uuid.so system/lib/libext2_uuid.so
adb pull /system/lib/libext2fs.so system/lib/libext2fs.so
adb pull /system/lib/libflint_engine_jni_api.so system/lib/libflint_engine_jni_api.so
adb pull /system/lib/libpicowrapper.so system/lib/libpicowrapper.so
adb pull /system/lib/libspeexwrapper.so system/lib/libspeexwrapper.so
adb pull /system/lib/libvideochat_jni.so system/lib/libvideochat_jni.so
adb pull /system/lib/libvideochat_stabilize.so system/lib/libvideochat_stabilize.so
adb pull /system/lib/libWVphoneAPI.so system/lib/libWVphoneAPI.so
adb pull /system/lib/libgcomm_jni.so system/lib/libgcomm_jni.so

cd tools
./extract-files.sh
cd ..

